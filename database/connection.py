import sys
import psycopg2
import psycopg2.extras
import psycopg2.extensions
import pprint
import sqlalchemy_pygresql

def main():
    conn_string = "host ='localhost' dbname='UIT' user='admin' password='admin' "
    print "connecting to database \ n->%s" %(conn_string)
    conn = psycopg2.connect(conn_string)

    # conn.cursor will return a cursor object, you can use this cursor to perform queries

    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    # tell postgres to use more work memory
    work_mem = 8192
    print "connected \n"

    cursor.execute ('SET work_mem TO %s',(work_mem,))

    cursor.execute ('SHOW work_mem')

    #fetch the first row return from the dtb
    memory = cursor.fetchone()

    print "Value: ",memory[0]

    print "Row: " ,memory

    #execute your query
    cursor.execute('SELECT * FROM BUILDING LIMIT 1000')
    row_count = 0
    for row in cursor:
        row_count += 1
        print "row: %s    %s\n" % (row_count, row)

    #retrieve the records from the database
    recordsBUILDING=cursor.fetchall()
    pprint.pprint(recordsBUILDING)
    # execute your query
    cursor.execute('SELECT * FROM SCHOOL LIMIT 1000')
    row_count = 0
    for row in cursor:
        row_count += 1
        print "row: %s    %s\n" % (row_count, row)

    # retrieve the records from the database
    recordsSCHOOL = cursor.fetchall()
    pprint.pprint(recordsSCHOOL)

    if __name__ == "__main__":
        main()
