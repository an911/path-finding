
import wx


########################################################################
class MyForm(wx.Frame):
    """"""

    # ----------------------------------------------------------------------
    def __init__(self):
        """Constructor"""
        wx.Frame.__init__(self, None, title="Path Finding")

        self.panel = wx.Panel(self, wx.ID_ANY)

        # Create menu bar
        menuBar = wx.MenuBar()

        # Create building menu
        buildingMenu = wx.Menu()
        buildingAItem = buildingMenu.Append(wx.NewId(), "building A",
                                    "path to building A",
                                    wx.ITEM_RADIO)
        buildingBItem = buildingMenu.Append(wx.NewId(), "building B",
                                    "path to building B",
                                    wx.ITEM_RADIO)
        buildingCItem = buildingMenu.Append(wx.NewId(), "building C",
                                    "path to building C",
                                    wx.ITEM_RADIO)
        buildingDItem = buildingMenu.Append(wx.NewId(), "building D",
                                    "path to building D",
                                    wx.ITEM_RADIO)
        buildingEItem = buildingMenu.Append(wx.NewId(), "building E",
                                    "path to building E",
                                    wx.ITEM_RADIO)
        canteenItem = buildingMenu.Append(wx.NewId(), "canteen",
                                    "path to cateen",
                                    wx.ITEM_RADIO)
        accomodationBuildingItem = buildingMenu.Append(wx.NewId(), "accomodation building",
                                    "path to accomodation building",
                                                       #the building behind the canteen
                                    wx.ITEM_RADIO)

        menuBar.Append(buildingMenu, "&building")

        # create campus menu

        #car park ,front gate ,back gate,front door intersection (the section between A building ,E building ,front gate, A-E-C intersection
        campusMenu = wx.Menu()
        carPackItem = campusMenu.Append(wx.NewId(), "Car Pack", "", wx.ITEM_CHECK)
        frontGateItem = campusMenu.Append(wx.NewId(), "Front gate", "", wx.ITEM_CHECK)
        backGatekItem = campusMenu.Append(wx.NewId(), "Back Gate", "", wx.ITEM_CHECK)
        frontDoorIntersectionItem = campusMenu.Append(wx.NewId(), "Front Door Intersection", "", wx.ITEM_CHECK)
        AECintersectionItem = campusMenu.Append(wx.NewId(), "A-E-C intersection", "", wx.ITEM_CHECK)
        menuBar.Append(campusMenu, "&campus")



        # Attach menu bar to frame
        self.SetMenuBar(menuBar)


# ----------------------------------------------------------------------
# Run the program
if __name__ == "__main__":
    app = wx.App(False)
    frame = MyForm().Show()
    app.MainLoop()