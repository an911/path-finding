import sys
import geopy

class RequestLocationToPois:
    def access_token(self,x):
        self.__x=x
    def __get__(self, x):
        return  self.__x
    def __set__(self, x):
        self.__x=x

    def build(self,y):
        self.__y=y
    def __getitem__(self,y):
        return self.__y
    def __setitem__(self, y):
        self.__y=y

    def _floor__number(self,x):
        assert isinstance(self,x)
        self.__x =x
    def __get__(self, x):
        return self.__x
    def __set__(self, x):
        self.__x=x

    def _pois_to (self,y):
        self.__y=y
    def __get__(self, y):
        return self.__y
    def __set__(self, y):
        self.__y=y

    def coordinates_lat (self,y):
        self.__y = y
    def __get__(self, y):
        return self.__y
    def __set__(self, y):
        self.__y=y

    def coordinates_lon(self,y):
        self.__y = y
    def __getitem__(self, y):
        return  self.__y
    def __setattr__(self, y):
        self.__y=y

